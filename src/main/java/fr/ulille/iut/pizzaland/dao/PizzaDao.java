package fr.ulille.iut.pizzaland.dao;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.Arrays;
import java.util.List;

public interface PizzaDao {

    @SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas(" +
            "id INTEGER," +
            "name varchar(50) UNIQUE NOT NULL," +
            "PRIMARY KEY (id));")
    void createPizzaTable();

    @SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (" +
            "pizza_id INTEGER NOT NULL ," +
            "ingredient_id INTEGER NOT NULL," +
            "PRIMARY KEY(pizza_id,ingredient_id)," +
            "FOREIGN KEY (pizza_id) REFERENCES Pizzas(id)," +
            "FOREIGN KEY (ingredient_id) REFERENCES ingredients(id));")
    void createAssociationTable();

    @SqlUpdate("DROP TABLE IF EXISTS Pizzas")
    void dropTable();

    @SqlUpdate("DELETE FROM Pizzas WHERE id = :id")
    void remove(long id);

    @SqlQuery("SELECT * FROM Pizzas WHERE id = :id")
    @RegisterBeanMapper(Pizza.class)
    Pizza findById(long id);

    @SqlQuery("SELECT * FROM Pizzas WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza findByName(String name);

    @SqlUpdate("INSERT INTO Pizzas (name) VALUES (:name)")
    @GetGeneratedKeys
    long insert(String name);


    @Transaction
    default void createTableAndIngredientAssociation() {
        createAssociationTable();
        createPizzaTable();
    }

    @SqlQuery("SELECT * FROM Pizzas")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getAll();
}

